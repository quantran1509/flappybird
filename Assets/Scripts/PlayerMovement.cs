﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public int score;
    private Rigidbody2D rb;
    private Animator anim;
    public float jumpForce = 1.5f;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetMouseButtonDown(0))
        {
          
            rb.velocity = Vector2.up * jumpForce;
            anim.Play("Fly");
        }
       
    }
    private void OnCollisionEnter2D(Collision2D target)
    {
         Debug.LogError("Dead");
         Death();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("ScorePipe"))
        {
            Debug.Log("Get Score");
            score++;
        }
        
    }
     void Death()
    {
        Time.timeScale = 0;
        FindObjectOfType<UIController>().highScore.gameObject.SetActive(true);
       
    }
}
