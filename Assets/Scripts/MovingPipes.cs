﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPipes : MonoBehaviour
{
    private float lifeTime;
    private float maxTime = 8f;
    private float speed = 1.5f;

    // Start is called before the first frame update
    private void Start()
    {
        lifeTime = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 temp = transform.position;
        temp.x -= speed * Time.deltaTime;
        transform.position = temp;

        lifeTime += Time.deltaTime;
        if (lifeTime > maxTime)
        {
            this.gameObject.SetActive(false);
            lifeTime = 0f;
        }
    }
    
}
