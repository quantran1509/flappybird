﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIController : MonoBehaviour
{
    public Text score;
    public Button button;
    public Text highScore;
    // Start is called before the first frame update
    void Start()
    {
        highScore.text = "HighScore: " + PlayerPrefs.GetInt("HighScore", 0).ToString();
        highScore.gameObject.SetActive(false);
        score.gameObject.SetActive(false);
        Time.timeScale = 0;
    }

    // Update is called once per frame
    void Update()
    {
        score.text = FindObjectOfType<PlayerMovement>().score.ToString();
        if (FindObjectOfType<PlayerMovement>().score > PlayerPrefs.GetInt("HighScore", 0))
        {
            PlayerPrefs.SetInt("HighScore", FindObjectOfType<PlayerMovement>().score);
            highScore.text = "HighScore: " + FindObjectOfType<PlayerMovement>().score.ToString();
        }
    }
    public void OnPlayButtonClick()
    {
        Time.timeScale = 1;
        button.gameObject.SetActive(false);
        score.gameObject.SetActive(true);
    }
}
