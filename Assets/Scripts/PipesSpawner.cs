﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipesSpawner : MonoBehaviour
{
    public Transform pipeSpawner;
    public float min_Y = -0.15f;
    public float max_Y = 4.53f;
    private float lifeTime;
    private float maxTime = 2f;
    private void Start()
    {
        lifeTime = 0f;


    }
    private void Update()
    {
        lifeTime += Time.deltaTime;
        if (lifeTime > maxTime)
        {
            SpawnPipes();
            lifeTime = 0;
        }
    }
    void SpawnPipes()
    {
        //Random y-axis
        float pos_Y = Random.Range(min_Y, max_Y);
        Vector3 temp = transform.position;
        temp.y = pos_Y;

        //Instantiate object at random cordinate 
        GameObject pipes = ObjectPooler.Instance.GetPooledObjects();
        if (pipes != null)
        {
            pipes.transform.position = temp;
            pipes.transform.rotation = Quaternion.identity;
            pipes.SetActive(true);
        }


    }
    

    
}
